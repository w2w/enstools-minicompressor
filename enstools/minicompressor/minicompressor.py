import xarray
from typing import Union, List
from enstools.encoding.api import DatasetEncoding


def read(*args, **kwargs):
    return xarray.open_mfdataset(*args, **kwargs)


def transfer_files(
        file_paths: List[str],
        output_folder: str,
        compression: str = "lossless",
        variables_to_keep: List[str] = None,
):
    """
    Iterates through a list of 
    """

    def new_file_path(file_path):
        return destination_path(file_path, output_folder)

    from joblib import Parallel, delayed
    Parallel(n_jobs=-1)(delayed(transfer_file)(file_path, new_file_path(
        file_path), compression, variables_to_keep) for file_path in file_paths)


def transfer_file(
        origin: str,
        destination: str,
        compression: str,
        variables_to_keep: List[str] = None
):
    """
    This function will copy a dataset while optionally applying compression.

    Parameters:
    ----------
    origin: string
            path to original file that will be copied.

    destination: string
            path to the new file that will be created.

    compression: string
            compression specification or path to json configuration file
    """
    with read(origin, decode_times=False) as dataset:
        if variables_to_keep is not None:
            dataset = drop_variables(dataset, variables_to_keep)

        # Create encoding
        encoding = DatasetEncoding(
            dataset=dataset, compression=compression)
        # Save file
        dataset.to_netcdf(destination, encoding=encoding, engine="h5netcdf")


def destination_path(origin_path: str, destination_folder: str):
    """
    Function to obtain the destination file from the source file and the destination folder.
    If the source file has GRIB format (.grb) , it will be changed to netCDF (.nc).

    Parameters
    ----------
    origin_path : string
            path to the original file

    destination_folder : string
            path to the destination folder

    Returns the path to the new file that will be placed in the destination folder.
    """
    from os.path import join, basename, splitext

    file_name = basename(origin_path)

    return join(destination_folder, file_name)


def drop_variables(dataset, variables_to_keep: list):
    """
    Drop all the variables that are not in list variables_to_keep.
    Keeps the coordinates.
    """
    # Drop the undesired variables and keep the coordinates
    coordinates = [v for v in dataset.coords]
    variables = [v for v in dataset.variables if v not in coordinates]
    variables_to_drop = [v for v in variables if v not in variables_to_keep]
    return dataset.drop_vars(variables_to_drop)
