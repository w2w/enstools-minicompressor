"""
Tests for the commandline interface.
"""

from os.path import isfile, join
from tempdir import TempDir

import pytest


def create_synthetic_dataset(directory):
    """
    Creates three synthetic netcdf datasets (1d,2d,3d) into the provided directory.
    :param directory:
    :return: None
    """
    import numpy as np
    import xarray as xr
    import pandas as pd
    # Create synthetic datasets
    nx, ny, nz, m, t = 30, 30, 5, 5, 5
    lon = np.linspace(-180, 180, nx)
    lat = np.linspace(-90, 90, ny)
    levels = np.array(range(nz))
    for dimension in [1, 2, 3, 4]:
        if dimension == 1:
            data_size = (t, nx)
            var_dimensions = ["time", "lon"]
        elif dimension == 2:
            data_size = (t, nx, ny)
            var_dimensions = ["time", "lon", "lat"]
        elif dimension == 3:
            data_size = (t, nz, nx, ny)
            var_dimensions = ["time", "level", "lon", "lat"]
        elif dimension == 4:
            data_size = (t, m, nz, nx, ny)
            var_dimensions = ["time", "ens", "level", "lon", "lat"]
        else:
            raise NotImplementedError()

        temp = 15 + 8 * np.random.randn(*data_size)
        precip = 10 * np.random.rand(*data_size)

        ds = xr.Dataset(
            {
                "temperature": (var_dimensions, temp),
                "precipitation": (var_dimensions, precip),
            },

            coords={
                "lon": lon,
                "lat": lat,
                "level": levels,
                "time": pd.date_range("2014-09-06", periods=t),
                "reference_time": pd.Timestamp("2014-09-05"),
            },
        )
        ds_name = "dataset_%iD.nc" % dimension
        ds.to_netcdf(join(directory, ds_name))


class TestClass:
    @classmethod
    def setup_class(cls):
        """
        This code will be executed at the beginning of the tests.
        We will be launching the
        :return:
        """
        """
        Creates two temporary directories:
        - Input directory: Will store the synthetic data created for the test
        - Output directory: Will store the compressed synthetic data
        :return: Tempdir, Tempdir
        """
        # Create temporary directory in which we'll put some synthetic datasets
        cls.input_tempdir = TempDir(check_free_space=False)
        cls.output_tempdir = TempDir(check_free_space=False)
        create_synthetic_dataset(cls.input_tempdir.getpath())

    @classmethod
    def teardown_class(cls):
        # release resources
        cls.input_tempdir.cleanup()
        cls.output_tempdir.cleanup()

    def test_dataset_exists(self):
        """
        Check that the data sets used for the test exist
        """
        input_tempdir = self.input_tempdir
        tempdir_path = input_tempdir.getpath()

        datasets = ["dataset_%iD.nc" % dimension for dimension in range(1, 4)]
        for ds in datasets:
            assert isfile(join(tempdir_path, ds))

    def test_help(self, mocker):
        """
        Check that the cli prints the help and exists.
        """
        import enstools.minicompressor
        commands = ["_", "-h"]
        mocker.patch("sys.argv", commands)
        with pytest.raises(SystemExit):
            enstools.minicompressor.cli()

    def test_compress(self, mocker):
        """
        Test enstools-compressor compress
        """
        import enstools.minicompressor
        input_tempdir = self.input_tempdir
        tempdir_path = input_tempdir.getpath()
        output_tempdir = self.output_tempdir
        output_folder = output_tempdir.getpath()

        file_name = "dataset_%iD.nc" % 3
        file_path = join(tempdir_path, file_name)
        output_file_path = join(output_folder, file_name)
        commands = ["_", file_path, "-o", output_folder]
        mocker.patch("sys.argv", commands)
        enstools.minicompressor.cli()
        assert isfile(output_file_path)

    def test_compress_with_compression_specification(self, mocker):
        """
        Test enstools-compressor compress
        """
        import enstools.minicompressor
        input_tempdir = self.input_tempdir
        tempdir_path = input_tempdir.getpath()
        output_tempdir = self.output_tempdir
        output_folder = output_tempdir.getpath()

        file_name = "dataset_%iD.nc" % 3
        file_path = join(tempdir_path, file_name)
        output_file_path = join(output_folder, file_name)
        compression = "temperature:lossy,zfp,rate,3.2 precipitation:lossy,zfp,rate,1.6 default:lossless"
        commands = ["_", file_path, "-o",
                    output_folder, "--compression", compression]
        mocker.patch("sys.argv", commands)
        enstools.minicompressor.cli()
        assert isfile(output_file_path)

    def test_compress_with_variable_drop(self, mocker):
        """
        Test enstools-compressor compress
        """
        import enstools.minicompressor
        input_tempdir = self.input_tempdir
        tempdir_path = input_tempdir.getpath()
        output_tempdir = self.output_tempdir
        output_folder = output_tempdir.getpath()

        file_name = "dataset_%iD.nc" % 3
        file_path = join(tempdir_path, file_name)
        output_file_path = join(output_folder, file_name)
        compression = "temperature:lossy,zfp,rate,3.2 default:lossless"
        commands = ["_", file_path,
                    "-o", output_folder,
                    "--compression", compression,
                    "--variables", "temperature"]
        mocker.patch("sys.argv", commands)
        enstools.minicompressor.cli()
        assert isfile(output_file_path)
