# [Ensemble Tools - MiniCompressor](https://github.com/wavestoweather/enstools-encoding)

Light-weight library to alloc compression using HDF5 filters through the enstools-encoding package. 

## Compressors
At the current stage it is possible to generate encodings for three compressors:
- [BLOSC](https://github.com/Blosc/hdf5-blosc)
- [ZFP](https://github.com/LLNL/H5Z-ZFP)
- [SZ](https://github.com/szcompressor/SZ)

While **BLOSC** and **ZFP** are available through the **hdf5plugin**, for **SZ** the filter needs to be installed separately.

## Quickstart
```bash
usage: minicompressor [-h] -o OUTPUT [--compression COMPRESSION [COMPRESSION ...]] [--variables VARIABLES] [files ...]

positional arguments:
  files                 Path to file/files that will be compressed.Multiple files and regex patterns are allowed.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
  --compression COMPRESSION [COMPRESSION ...]
                        Specifications about the compression options. Default is: lossless
  --variables VARIABLES
                        List of variables to be kept. The other variables will be dropped.Must be a list of comma separated values: i.e. vor,temp,qvDefault=None

```
## Installation using pip

`pip` is the easiest way to install `enstools-minicompressor` along with all dependencies:

```
pip install git+https://gitlab.physik.uni-muenchen.de/w2w/enstools-minicompressor.git
```


## Compression Specification Format

We defined our own specification format to represent the compression parameters that we want to apply to the data.
One example looks like this:

```
lossy,zfp,rate,4.0
```

Its purpose is to represent the specifications in a way that is easy to understand and use.

The currently implemented compressors include Blosc, for lossless compression, and ZFP and SZ for lossy compression.
For lossless compression, one can simply use:

```
lossless
```

This will use the default backend **lz4** with compression level 9.
It is also possible to select a different backend (blosclz, lz4, lz4hc,snappy,zlib or zstd) or compression level (1 to
9):

```
lossless,snappy,9
```

For lossy compression, it is mandatory to include the compressor, the mode and the parameter. Few examples:

```
lossy,sz,abs,0.01
lossy,zfp,rate,4.0
lossy,sz,rel,1e-3
lossy,zfp,precision,10
lossy,sz,pw_rel,0.05
lossy,zfp,accuracy,0.01
```

There are also few features that target datasets with multiple variables.
One can write a different specification for different variables by using a list of space separated specifications:

```
var1:lossy,zfp,rate,4.0 var2:lossy,sz,abs,0.1
```

It is possible too to specify the default value for the variables that are not explicitly mentioned:

```
var1:lossy,zfp,rate,4.0 default:lossy,sz,abs,0.1
```

In case a specification doesn't have a variable name, it will be considered the default. i.e:

`var1:lossy,zfp,rate,4.0 lossy,sz,abs,0.1` -> `var1:lossy,zfp,rate,4.0 default:lossy,sz,abs,0.1`

If no default value is provided, lossless compression will be applied:

`var1:lossy,zfp,rate,4.0` ->  `var1:lossy,zfp,rate,4.0 default:lossless`

Coordinates are treated separately, by default are compressed using `lossless`, although it is possible to change that:

`coordinates:lossy,zfp,rate,6`

# Acknowledgment and license

Ensemble Tools - Minicompressor (`enstools-minicompressor`) is a collaborative development within
Waves to Weather (SFB/TRR165) coordinated by the subproject 
[Z2](https://www.wavestoweather.de/research_areas/phase2/z2) and funded by the
German Research Foundation (DFG).

A full list of code contributors can [CONTRIBUTORS.md](./CONTRIBUTORS.md).

The code is released under an [Apache-2.0 licence](./LICENSE).
