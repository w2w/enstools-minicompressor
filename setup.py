"""
Setup file for enstools-encoding
"""
from setuptools import setup

# Use the Readme file as long description.
try:
    with open("Readme.md", "r") as f:
        long_description = f.read()
except FileNotFoundError:
    long_description = ""


# perform the actual install operation
setup(name="enstools-minicompressor",
      version="0.1.0",
      author="Oriol Tintó",
      author_email="oriol.tinto@lmu.de",
      long_description=long_description,
      long_description_content_type='text/markdown',
      url="https://github.com/wavestoweather/enstools-minicompressor",
      packages=["enstools.minicompressor"],
      namespace_packages=['enstools'],

      install_requires=[
          "enstools-encoding",
          "hdf5plugin>=4.0.0",
          "dask",
          "netCDF4",
          "joblib",
      ],
      entry_points={
          'console_scripts': [
              'minicompressor=enstools.minicompressor:cli'
          ],
      },
      )
